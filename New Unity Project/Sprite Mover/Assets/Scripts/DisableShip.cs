﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableShip : MonoBehaviour {

    private GameObject theShip; // Create variable for game object to set inactive
    private SpriteMover mover; // Create variable for component to disable

	// Use this for initialization
	void Start () {
        theShip = this.gameObject;
        mover = GetComponent<SpriteMover>(); //Get access to component
	}
	
	// Update is called once per frame
	void Update () {
		//If player presses P key disable movement
        if (Input.GetKeyDown(KeyCode.P))
        {
            mover.enabled = false;
        }//If player presses L key movement is enabled
        else if (Input.GetKeyDown(KeyCode.L))
        {
            mover.enabled = true;
        }

        //If player presses Q key set game object to inactive
        if (Input.GetKeyDown(KeyCode.Q))
        {
            theShip.SetActive(false);
        }
      
	}
}
