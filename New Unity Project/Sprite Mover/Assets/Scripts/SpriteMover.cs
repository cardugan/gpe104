﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteMover : MonoBehaviour {

    private Transform tf; //Create variable for Transform component
    public float speed = 0.25f; //Create variable for speed

	// Use this for initialization
	void Start () {
        tf = GetComponent<Transform>(); //Get access to Transform component
	}
	
	// Update is called once per frame
	void Update () {
		//Check for player inputs
        //If player presses arrow key, move that direction continuously
        //Move toward right of screen
        if (Input.GetAxis("horizontal") > 0 && !Input.GetButton("step"))
        {
            tf.position += (new Vector3(1, 0, 0) * speed);
        } //Move toward left of screen
        else if (Input.GetAxis("horizontal") < 0 && !Input.GetButton("step"))
        {
            tf.position -= (new Vector3(1, 0, 0) * speed);
        } //Move toward top of screen
        if (Input.GetAxis("vertical") > 0 && !Input.GetButton("step"))
        {
            tf.position += (new Vector3(0, 1, 0) * speed);
        } //Move toward the bottom of screen
        else if (Input.GetAxis("vertical") < 0 && !Input.GetButton("step"))
        {
            tf.position -= (new Vector3(0, 1, 0) * speed);
        }

        //If player presses shift + arrow key, move one unit that direction
        //Move right
        if (Input.GetButton("step") && Input.GetKeyDown(KeyCode.RightArrow)) {
            tf.position += new Vector3(1, 0, 0);
        } //Move left
        else if (Input.GetButton("step") && Input.GetKeyDown(KeyCode.LeftArrow))
        {
            tf.position -= new Vector3(1, 0, 0);
        } //Move up
        else if (Input.GetButton("step") && Input.GetKeyDown(KeyCode.UpArrow))
        {
            tf.position += new Vector3(0, 1, 0);
        } //Move down
        else if (Input.GetButton("step") && Input.GetKeyDown(KeyCode.DownArrow))
        {
            tf.position -= new Vector3(0, 1, 0);
        }

        //If player presses space bar, move to center
        if (Input.GetKeyDown(KeyCode.Space))
        {
            tf.position = Vector3.zero;
        }

        //If player presses tab key, ship's front end reverses direction
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            tf.localScale = -tf.localScale; //This works for this sprite, but how would I specify flipping x only?
        }
    }
}
