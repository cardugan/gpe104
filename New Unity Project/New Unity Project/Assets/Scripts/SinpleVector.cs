﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinpleVector : MonoBehaviour {

    float myVector1 = -3.0f; //A one-dimensional vector is just a float

	// Use this for initialization
	void Start () {
        Debug.Log(Mathf.Abs(myVector1)); //Output the magnitude
        Debug.Log(Mathf.Sign(myVector1)); //Output the direction

        Vector2 myVector = new Vector2(2, -2); //Create a vector 2 units on x, -2 units on y
        Debug.Log(myVector.magnitude); //Output the magnitude

        Vector3 myVector3 = new Vector3(2, -2, 27); //Create a vector 2 units on x, -2 on y, 27 on z
        Debug.Log(myVector3.magnitude); //Output the magnitude
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
