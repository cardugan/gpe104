﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleMovement : MonoBehaviour {

    private Transform tf; //A variable to hold the Transform component
    public float speed = 0.5f;

	// Use this for initialization
	void Start () {
        //Get the transform component
        tf = GetComponent<Transform>(); 
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 myVector = new Vector3(2, 4, 12);
        myVector = myVector.normalized; //Changes magnitude to 1 (also myVector.Normalize())
        //Move up every frame draw by adding 1 to the y of our position
        tf.position = tf.position + (myVector * speed); 
        //Vector3.up is a preset vector of (0,1,0)
        //There is also a Vector3.right (1,0,0) and Vector3.forward (0,0,1)
	}
}
