﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMover : MonoBehaviour {

    public float maxSpeed = 5f;
    private Transform tf;
    

    // Use this for initialization
    void Start()
    {
        tf = gameObject.GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {

        Vector3 position = tf.position;
        Vector3 velocity = new Vector3(0, maxSpeed * Time.deltaTime, 0);
        position += tf.rotation * velocity;
        tf.position = position;

    }
}
