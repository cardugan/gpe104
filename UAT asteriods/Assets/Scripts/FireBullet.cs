﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBullet : MonoBehaviour {

    //Create variable for prefab bullets
    public GameObject pfBullet;
    //Create variable for transform component for instantiation
    private Transform tf;
    //Create variable for time to bullet self-destruct
    public float bulletTime = 2.0f;
    
    
   

    public void Fire(float destructTime)
    {
        //Create variable to store instance of bullet
        GameObject bullet;

        Vector3 offset = tf.localRotation * new Vector3(0, 1.0f, 0);

        //Instantiate (what, where, what rotation)
        bullet = Instantiate(pfBullet, tf.position + offset, tf.rotation) as GameObject;

        //Bullet self-destruct after an amount of time
        Destroy(bullet, destructTime);
    }

    // Use this for initialization
    void Start () {
        tf = GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Fire(bulletTime);
        }
    }
}
